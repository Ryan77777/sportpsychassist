import IcCircleWarning from './IcCircleWarning.svg';
import IcArrowRight from './IcArrowRight.svg';
import IcArrowLeft from './IcArrowLeft.svg';
import IcHome from './IcHome.svg';
import IcHomeActive from './IcHomeActive.svg';
import IcUser from './IcUser.svg';
import IcUserActive from './IcUserActive.svg';
import IcCheck from './IcCheck.svg';
import IcCamera from './IcCamera.svg';
import IcTrash from './IcTrash.svg';
import IcPointOnMapActive from './IcPointOnMapActive.svg';
import IcPointOnMap from './IcPointOnMap.svg';

export {
  IcCircleWarning,
  IcArrowRight,
  IcArrowLeft,
  IcHome,
  IcHomeActive,
  IcUser,
  IcUserActive,
  IcCheck,
  IcCamera,
  IcTrash,
  IcPointOnMap,
  IcPointOnMapActive,
};
