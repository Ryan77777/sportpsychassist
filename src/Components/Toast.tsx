import React from 'react';
import {StyleSheet, View} from 'react-native';
import Animated, {BounceInLeft, BounceOutLeft} from 'react-native-reanimated';
import {IcCircleWarning} from '../Assets';
import {ToastProps, ToastState} from '../Models/ComponentModels';
import {Colors} from '../Themes/Colors';
import {scaleSize} from '../Utils/Normalize';
import ToastHelper from '../Utils/ToastHelper';
import Gap from './Gap';
import {TextMedium, TextRegular} from './Text';

class Toast extends React.Component<ToastProps, ToastState> {
  static instance: Toast | null = null;

  constructor(props: ToastProps) {
    super(props);
    this.state = {
      visible: false,
      title: '',
      message: '',
    };
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
  }

  componentDidMount(): void {
    ToastHelper.setInstance({show: this.show, hide: this.hide});
  }

  show(title: string, message: string) {
    this.setState({visible: true, title, message});

    setTimeout(() => {
      this.hide();
    }, 4000);
  }

  hide() {
    this.setState({visible: false, title: '', message: ''});
  }

  render() {
    const {visible, title, message} = this.state;
    return (
      visible && (
        <Animated.View
          entering={BounceInLeft.duration(1000)}
          exiting={BounceOutLeft.duration(1000)}
          style={styles.toast}>
          <IcCircleWarning />
          <Gap width={8} />
          <View>
            <TextMedium type="M" text={title} color={Colors['Error-600']} />
            <Gap height={4} />
            <TextRegular
              type="S"
              text={message}
              color={Colors['Error-500']}
              style={styles.text}
            />
          </View>
        </Animated.View>
      )
    );
  }
}

export default Toast;

const styles = StyleSheet.create({
  toast: {
    position: 'absolute',
    bottom: scaleSize(43),
    left: scaleSize(20),
    right: scaleSize(20),
    backgroundColor: Colors['Error-50'],
    borderWidth: 1,
    borderColor: Colors['Error-200'],
    borderRadius: scaleSize(12),
    padding: scaleSize(16),
    zIndex: 1,
    flexDirection: 'row',
  },
  text: {
    width: '85%',
  },
});
