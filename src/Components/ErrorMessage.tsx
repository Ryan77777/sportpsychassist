import React from 'react';
import Animated, {BounceInLeft, BounceOutLeft} from 'react-native-reanimated';
import {ErrorMessageProps} from '../Models/ComponentModels';
import {Colors} from '../Themes/Colors';
import {TextRegular} from './Text';

const ErrorMessage: React.FC<ErrorMessageProps> = ({text, isError}) => {
  if (isError && typeof text === 'string') {
    return (
      <Animated.View
        entering={BounceInLeft.duration(1000)}
        exiting={BounceOutLeft.duration(1000)}>
        <TextRegular type="S" text={text} color={Colors['Error-500']} />
      </Animated.View>
    );
  }

  return null;
};

export default ErrorMessage;
