import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Colors} from '../Themes/Colors';
import {scaleSize} from '../Utils/Normalize';
import {IcCheck} from '../Assets';

const Checkbox: React.FC<any> = ({isCheck, onPress}) => {
  const backgroundColor = isCheck ? Colors['Sky-600'] : 'transparent';
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={[
        styles.checkBox,
        {
          backgroundColor,
          borderColor: isCheck ? Colors['Primary-500'] : Colors['Neutral-300'],
        },
      ]}
      onPress={onPress}>
      <IcCheck />
    </TouchableOpacity>
  );
};

export default Checkbox;

const styles = StyleSheet.create({
  checkBox: {
    width: scaleSize(20),
    height: scaleSize(20),
    borderWidth: 1,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
