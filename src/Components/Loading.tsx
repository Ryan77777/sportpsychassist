import React from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {Colors} from '../Themes/Colors';
import LoadingHelper from '../Utils/LoadingHelper';
import {LoadingProps, LoadingState} from '../Models/ComponentModels';

class Loading extends React.Component<LoadingProps, LoadingState> {
  constructor(props: LoadingProps) {
    super(props);
    this.state = {
      visible: false,
    };
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
  }

  componentDidMount(): void {
    LoadingHelper.setInstance({show: this.show, hide: this.hide});
  }

  show() {
    this.setState({visible: true});
  }

  hide() {
    this.setState({visible: false});
  }

  render() {
    const {visible} = this.state;
    return (
      visible && (
        <View style={styles.page}>
          <ActivityIndicator size="large" color={Colors['Sky-600']} />
        </View>
      )
    );
  }
}

export default Loading;

const styles = StyleSheet.create({
  page: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0.5)',
    zIndex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
