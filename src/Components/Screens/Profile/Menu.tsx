import React from 'react';
import {SectionList, StyleSheet, View} from 'react-native';
import {MenuSection} from '../../../Models/ComponentModels';
import {Colors} from '../../../Themes/Colors';
import {scaleSize} from '../../../Utils/Normalize';
import Gap from '../../Gap';
import {TextMedium, TextSemiBold} from '../../Text';
import {DATA} from '../../../Data/MenuProfile';
import {IcArrowRight} from '../../../Assets';

const renderItem = ({
  item,
  index,
  section,
}: {
  item: string;
  index: number;
  section: MenuSection;
}) => {
  const isLastItem = index === section.data.length - 1;

  const borderBottomWidth = isLastItem ? 0 : 0.5;

  return (
    <View
      style={[
        styles.item,
        {borderBottomWidth, borderBottomColor: Colors['Neutral-200']},
      ]}>
      <TextMedium type="M" text={item} color={Colors['Neutral-800']} />
      <IcArrowRight />
    </View>
  );
};

const renderSectionHeader = ({section: {title}}: {section: MenuSection}) => (
  <TextSemiBold type="L" text={title} color={Colors['Neutral-600']} />
);

const renderSeparator = (isLastItem: boolean) => {
  return isLastItem ? null : <Gap height={16} />;
};

const renderSectionSeparator = ({leadingItem}: {leadingItem: string}) => {
  const isLastSection = DATA[DATA.length - 1].data.includes(leadingItem);

  return isLastSection ? null : <Gap height={16} />;
};

const Menu = () => {
  return (
    <View style={styles.card}>
      <SectionList
        sections={DATA}
        nestedScrollEnabled
        scrollEnabled={false}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => item + index}
        renderItem={renderItem}
        renderSectionHeader={renderSectionHeader}
        ItemSeparatorComponent={({leadingItem}) =>
          renderSeparator(leadingItem === undefined)
        }
        SectionSeparatorComponent={({leadingItem}) =>
          renderSectionSeparator({leadingItem})
        }
      />
    </View>
  );
};

export default Menu;

const styles = StyleSheet.create({
  card: {
    flex: 1,
    borderWidth: 0.5,
    borderColor: Colors['Neutral-200'],
    borderRadius: scaleSize(12),
    padding: scaleSize(16),
    flexDirection: 'row',
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: scaleSize(16),
  },
});
