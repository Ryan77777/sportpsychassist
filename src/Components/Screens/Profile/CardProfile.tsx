import React from 'react';
import {StyleSheet, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {Colors} from '../../../Themes/Colors';
import {scaleSize} from '../../../Utils/Normalize';
import Gap from '../../Gap';
import {TextBold, TextRegular} from '../../Text';

const CardProfile = () => {
  return (
    <View style={styles.card}>
      <FastImage
        style={styles.profile}
        source={{
          uri: 'https://ui-avatars.com/api/?name=Ahmad+Aceng',
          priority: FastImage.priority.normal,
          cache: FastImage.cacheControl.cacheOnly,
        }}
        resizeMode={FastImage.resizeMode.contain}
      />
      <Gap width={12} />
      <View>
        <TextBold type="L" text="Ahmad Aceng" color={Colors['Neutral-800']} />
        <Gap height={4} />
        <TextRegular
          type="S"
          text="dummy@gmail.com"
          color={Colors['Neutral-600']}
        />
      </View>
    </View>
  );
};

export default CardProfile;

const styles = StyleSheet.create({
  card: {
    borderWidth: 0.5,
    borderColor: Colors['Neutral-200'],
    borderRadius: scaleSize(12),
    padding: scaleSize(16),
    flexDirection: 'row',
  },
  profile: {
    width: scaleSize(40),
    height: scaleSize(40),
    borderRadius: scaleSize(20),
  },
});
