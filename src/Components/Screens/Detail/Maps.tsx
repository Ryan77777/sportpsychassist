import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {MapsProps} from '../../../Models/ComponentModels';

const Maps: React.FC<MapsProps> = ({latitude, longitude}) => {
  return (
    <MapView
      style={styles.map}
      region={{
        latitude,
        longitude,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
      }}>
      <Marker
        coordinate={{latitude, longitude}}
        title="Lokasi Pelapor"
        description="Contoh Laporan"
      />
    </MapView>
  );
};

export default Maps;

const styles = StyleSheet.create({
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height * 0.4,
    zIndex: 0,
  },
});
