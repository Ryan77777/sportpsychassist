import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../../../Themes/Colors';
import {scaleSize} from '../../../Utils/Normalize';
import Badge from '../../Badge';
import Gap from '../../Gap';
import {TextBold} from '../../Text';
import {ILBanner} from '../../../Assets';

const Banner = () => {
  return (
    <View style={styles.card}>
      <View>
        <Badge text="Pengumuman" />
        <Gap height={12} />
        <TextBold type="L" text="Pelaporan Menjadi" />
        <Gap height={4} />
        <TextBold type="L" text="Lebih Akurat" />
      </View>
      <View style={styles.ill}>
        <ILBanner />
      </View>
    </View>
  );
};

export default Banner;

const styles = StyleSheet.create({
  card: {
    borderWidth: 0.5,
    borderColor: Colors['Neutral-200'],
    borderRadius: scaleSize(12),
    padding: scaleSize(16),
    flexDirection: 'row',
    height: scaleSize(152, 'height'),
  },
  ill: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
});
