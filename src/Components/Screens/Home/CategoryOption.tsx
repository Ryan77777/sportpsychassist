import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Colors} from '../../../Themes/Colors';
import {scaleSize} from '../../../Utils/Normalize';
import {TextMedium} from '../../Text';
import {CategoryOptionProps} from '../../../Models/ComponentModels';

const CategoryOption: React.FC<CategoryOptionProps> = ({
  text,
  isSelected,
  onPress,
}) => {
  const borderWidth = 0.5;
  const borderColor =
    isSelected === text ? 'transparent' : Colors['Neutral-200'];

  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={[
        styles.option,
        {
          backgroundColor:
            isSelected === text ? Colors['Sky-50'] : Colors.White,
          borderWidth,
          borderColor,
        },
      ]}
      onPress={onPress}>
      <TextMedium
        type="M"
        text={text}
        color={isSelected === text ? Colors['Sky-600'] : Colors['Neutral-600']}
      />
    </TouchableOpacity>
  );
};

export default React.memo(CategoryOption);

const styles = StyleSheet.create({
  option: {
    paddingHorizontal: scaleSize(16),
    paddingVertical: scaleSize(12),
    borderRadius: scaleSize(99),
    marginRight: scaleSize(10),
  },
});
