import React, {useCallback, useState} from 'react';
import {
  FlatList,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import Animated, {FadeInDown} from 'react-native-reanimated';
import {Colors} from '../../../Themes/Colors';
import NavigationServices from '../../../Utils/NavigationServices';
import {scaleSize} from '../../../Utils/Normalize';
import Gap from '../../Gap';
import {TextMedium, TextRegular, TextSemiBold} from '../../Text';
import CategoryOption from './CategoryOption';

const categories = ['Semua', 'Evaluasi Awal', 'Pemecahan Masalah'];

const renderSeparator = () => <Gap height={16} />;

const SectionReport = () => {
  const [isSelected, setIsSelected] = useState('Semua');
  const [data, setData] = useState([0, 1, 2, 3, 4, 5, 6]);

  const filterDataByCategory = useCallback((category: string) => {
    if (category === 'Semua') {
      setData([0, 1, 2, 3, 4, 5, 6]);
    } else if (category === 'Evaluasi Awal') {
      setData([0, 2, 4]);
    } else if (category === 'Pemecahan Masalah') {
      setData([3, 5, 6]);
    }
  }, []);

  const redirectTo = () => {
    NavigationServices.navigate('DetailScreen');
  };

  const renderItem = useCallback(
    ({item, index}: {item: any; index: number}) => {
      return (
        <Animated.View
          style={styles.card}
          entering={FadeInDown.delay(500 * index).duration(500)}>
          <TouchableOpacity activeOpacity={0.7} onPress={redirectTo}>
            <FastImage
              style={styles.thumbnail}
              source={{
                uri: 'https://picsum.photos/1024/768',
                priority: FastImage.priority.high,
              }}
              resizeMode={FastImage.resizeMode.cover}
            />
          </TouchableOpacity>
          <View style={styles.content}>
            <TextSemiBold type="S" text={`Judul ${item + 1}`} />
            <Gap height={4} />
            <TextRegular
              type="XS"
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac dolor sit amet purus dignissim ultricies. Integer euismod bibendum libero, at malesuada nunc facilisis at. Fusce a dolor eget erat auctor gravida eu id libero. Nunc vel ex nec libero faucibus interdum. In hac habitasse platea dictumst."
              color={Colors['Neutral-500']}
              numberOfLines={2}
            />
            <Gap height={8} />
            <View style={styles.sender}>
              <FastImage
                style={styles.profile}
                source={{
                  uri: 'https://ui-avatars.com/api/?name=Joko+Susilo',
                  priority: FastImage.priority.high,
                }}
                resizeMode={FastImage.resizeMode.cover}
              />
              <Gap width={6} />
              <TextMedium type="S" text="Joko Susilo" />
            </View>
          </View>
        </Animated.View>
      );
    },
    [],
  );

  const renderCategory = useCallback(() => {
    return (
      <>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {categories.map((item, index) => (
            <CategoryOption
              key={index}
              text={item}
              isSelected={isSelected}
              onPress={() => {
                setIsSelected(item);
                filterDataByCategory(item);
              }}
            />
          ))}
        </ScrollView>
        <Gap height={16} />
      </>
    );
  }, [isSelected, filterDataByCategory]);

  return (
    <View>
      <FlatList
        nestedScrollEnabled
        numColumns={2}
        keyExtractor={item => item.toString()}
        showsVerticalScrollIndicator={false}
        scrollEnabled={false}
        data={data}
        renderItem={renderItem}
        contentContainerStyle={styles.container}
        ItemSeparatorComponent={renderSeparator}
        ListHeaderComponent={renderCategory}
        columnWrapperStyle={styles.contentWrapper}
      />
    </View>
  );
};

export default SectionReport;

const styles = StyleSheet.create({
  container: {
    paddingBottom: Platform.OS === 'ios' ? scaleSize(64) : scaleSize(80),
  },
  contentWrapper: {
    justifyContent: 'space-between',
  },
  card: {
    width: '48%',
    backgroundColor: Colors.White,
    borderRadius: scaleSize(16),
    borderWidth: 0.5,
    borderColor: Colors['Neutral-200'],
  },
  thumbnail: {
    width: '100%',
    height: scaleSize(100),
    borderTopLeftRadius: scaleSize(16),
    borderTopRightRadius: scaleSize(16),
  },
  content: {
    padding: scaleSize(10),
  },
  sender: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  profile: {
    width: scaleSize(16),
    height: scaleSize(16),
    borderRadius: scaleSize(8),
  },
});
