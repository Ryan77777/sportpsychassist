import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {FABProps} from '../Models/ComponentModels';
import {Colors} from '../Themes/Colors';
import {scaleSize} from '../Utils/Normalize';
import {TextRegular} from './Text';

const FAB: React.FC<FABProps> = ({onPress}) => {
  return (
    <TouchableOpacity activeOpacity={0.7} style={styles.fab} onPress={onPress}>
      <TextRegular type="XL" text="+" color={Colors.White} />
    </TouchableOpacity>
  );
};

export default FAB;

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    bottom: scaleSize(64),
    right: scaleSize(32),
    backgroundColor: Colors['Sky-600'],
    width: scaleSize(48),
    height: scaleSize(48),
    borderRadius: scaleSize(40),
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 99,
    shadowColor: Colors.Black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});
