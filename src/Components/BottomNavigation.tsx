import React from 'react';
import {Platform, StyleSheet, TouchableOpacity, View} from 'react-native';
import {
  IcHome,
  IcHomeActive,
  IcPointOnMap,
  IcPointOnMapActive,
  IcUser,
  IcUserActive,
} from '../Assets';
import {
  BottomNavigationProps,
  IconBottomNavigationProps,
} from '../Models/ComponentModels';
import {Colors} from '../Themes/Colors';
import {scaleSize} from '../Utils/Normalize';
import Gap from './Gap';
import {TextMedium, TextSemiBold} from './Text';

const Icon: React.FC<IconBottomNavigationProps> = ({label, active}) => {
  switch (label) {
    case 'Home':
      return active ? <IcHomeActive /> : <IcHome />;
    case 'Maps':
      return active ? <IcPointOnMapActive /> : <IcPointOnMap />;
    case 'Profile':
      return active ? <IcUserActive /> : <IcUser />;
    default:
      return <IcHome />;
  }
};

const BottomNavigation: React.FC<BottomNavigationProps> = ({
  state,
  descriptors,
  navigation,
}) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View>
      <View style={styles.container}>
        {state.routes.map((route: any, index: number) => {
          const {options} = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          return (
            <TouchableOpacity
              key={index}
              activeOpacity={0.7}
              accessibilityRole="button"
              accessibilityState={isFocused ? {selected: true} : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={styles.menu}>
              <Icon label={label} active={isFocused} />
              <Gap height={6} />
              {isFocused ? (
                <TextSemiBold type="S" text={label} color={Colors['Sky-600']} />
              ) : (
                <TextMedium
                  type="S"
                  text={label}
                  color={Colors['Neutral-400']}
                />
              )}
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
};

export default BottomNavigation;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    borderTopLeftRadius: scaleSize(24),
    borderTopRightRadius: scaleSize(24),
    backgroundColor: Colors.White,
    shadowColor: Colors.Black,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
  menu: {
    flex: 1,
    marginBottom: Platform.OS === 'ios' ? scaleSize(16) : 0,
    height: Platform.OS === 'android' ? scaleSize(56) : scaleSize(64),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
