import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {FilledButtonProps} from '../Models/ComponentModels';
import {Colors} from '../Themes/Colors';
import {scaleSize} from '../Utils/Normalize';
import {TextSemiBold} from './Text';

export const FilledButton: React.FC<FilledButtonProps> = ({
  text,
  paddingVertical = scaleSize(12),
  borderRadius = scaleSize(12),
  disabled,
  onPress,
}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      activeOpacity={0.7}
      style={[
        styles.button,
        {
          backgroundColor: disabled ? Colors['Neutral-300'] : Colors['Sky-600'],
          paddingVertical,
          borderRadius,
        },
      ]}
      onPress={onPress}>
      <TextSemiBold type="L" text={text} color={Colors.White} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
