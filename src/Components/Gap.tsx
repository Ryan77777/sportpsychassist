import React from 'react';
import {View} from 'react-native';
import {GapProps} from '../Models/ComponentModels';
import {scaleSize} from '../Utils/Normalize';

const Gap: React.FC<GapProps> = ({width = 0, height = 0}) => {
  return (
    <View
      style={{
        width: scaleSize(width),
        height: scaleSize(height),
      }}
    />
  );
};

export default Gap;
