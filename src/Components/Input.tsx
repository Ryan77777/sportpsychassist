import React, {forwardRef, useState} from 'react';
import {Controller} from 'react-hook-form';
import {StyleSheet, TextInput, View} from 'react-native';
import {InputProps} from '../Models/ComponentModels';
import {Colors} from '../Themes/Colors';
import {scaleFont, scaleSize} from '../Utils/Normalize';
import ErrorMessage from './ErrorMessage';
import Gap from './Gap';
import {TextMedium} from './Text';

const Input = forwardRef<TextInput, InputProps>((props, ref) => {
  const [borderWidth, setBorderWidth] = useState(0);
  const [borderColor, setBorderColor] = useState('transparent');

  const onFocus = () => {
    setBorderWidth(1);
    setBorderColor(Colors['Sky-600']);
  };

  const onBlur = () => {
    setBorderWidth(0);
    setBorderColor(borderColor);
  };

  return (
    <Controller
      control={props.control}
      name={props.name}
      rules={props.rules || {}}
      render={({field: {onChange, value}, formState: {errors}}) => {
        const isError = !!errors[props.name];

        const dynamicBorderWidth = isError ? 1 : borderWidth;

        const dynamicBorderColor = isError ? Colors['Error-600'] : borderColor;

        return (
          <>
            <TextMedium
              type="M"
              text={props.label}
              color={isError ? Colors['Error-600'] : Colors['Neutral-800']}
            />
            <Gap height={4} />
            <View
              style={[
                styles.container,
                {
                  borderWidth: dynamicBorderWidth,
                  borderColor: dynamicBorderColor,
                },
              ]}>
              <TextInput
                {...props}
                ref={ref}
                value={value}
                style={styles.input}
                placeholderTextColor={Colors['Neutral-400']}
                onFocus={onFocus}
                onBlur={onBlur}
                onChangeText={onChange}
              />
            </View>
            <Gap height={4} />
            <ErrorMessage
              isError={isError}
              text={errors?.[props.name]?.message}
            />
          </>
        );
      }}
    />
  );
});

export default Input;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: scaleSize(50),
    paddingHorizontal: scaleSize(12),
    paddingVertical: scaleSize(16),
    borderRadius: scaleSize(12),
    backgroundColor: Colors['Neutral-50'],
  },
  input: {
    flex: 1,
    height: scaleSize(50),
    fontFamily: 'Inter-Regular',
    fontSize: scaleFont(14),
    color: Colors['Neutral-800'],
    lineHeight: 20,
  },
});
