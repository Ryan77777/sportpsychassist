import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {scaleFont} from '../Utils/Normalize';
import {Colors} from '../Themes/Colors';
import {TextProps} from '../Models/ComponentModels';

const getSize = (type: TextProps['type']) => {
  switch (type) {
    case '6XL':
      return 60;
    case '5XL':
      return 48;
    case '4XL':
      return 36;
    case '3XL':
      return 32;
    case '2XL':
      return 24;
    case 'XL':
      return 20;
    case 'L':
      return 16;
    case 'M':
      return 14;
    case 'S':
      return 12;
    case 'XS':
      return 10;
  }
};

const getLineHeight = (size: number) => {
  switch (size) {
    case 60:
      return size * 1.2;
    case 48:
      return size * 1.2;
    case 36:
      return size * 1.4;
    case 32:
      return size * 1.4;
    case 24:
      return size * 1.4;
    case 20:
      return size * 1.6;
    case 16:
      return size * 1.6;
    case 14:
      return size * 1.6;
    case 12:
      return size * 1.6;
    case 10:
      return size * 1.6;
  }
};

export const TextBold: React.FC<TextProps> = ({
  style,
  type,
  text,
  color = Colors['Neutral-800'],
  numberOfLines,
}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text
      style={[
        styles.bold,
        {fontSize: scaleFont(size), color, lineHeight},
        style,
      ]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

export const TextSemiBold: React.FC<TextProps> = ({
  style,
  type,
  text,
  color = Colors['Neutral-800'],
  numberOfLines,
}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text
      style={[
        styles.semibold,
        {fontSize: scaleFont(size), color, lineHeight},
        style,
      ]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

export const TextMedium: React.FC<TextProps> = ({
  style,
  type,
  text,
  color = Colors['Neutral-800'],
  numberOfLines,
}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text
      style={[
        styles.medium,
        {fontSize: scaleFont(size), color, lineHeight},
        style,
      ]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

export const TextRegular: React.FC<TextProps> = ({
  style,
  type,
  text,
  color = Colors['Neutral-800'],
  numberOfLines,
}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text
      style={[
        styles.regular,
        {fontSize: scaleFont(size), color, lineHeight},
        style,
      ]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

export const TextLight: React.FC<TextProps> = ({
  style,
  type,
  text,
  color = Colors['Neutral-800'],
  numberOfLines,
}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text
      style={[
        styles.light,
        {fontSize: scaleFont(size), color, lineHeight},
        style,
      ]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

const styles = StyleSheet.create({
  bold: {
    fontFamily: 'Inter-Bold',
  },
  semibold: {
    fontFamily: 'Inter-SemiBold',
  },
  medium: {
    fontFamily: 'Inter-Medium',
  },
  regular: {
    fontFamily: 'Inter-Regular',
  },
  light: {
    fontFamily: 'Inter-Light',
  },
});
