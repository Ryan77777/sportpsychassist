import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../Themes/Colors';
import {scaleSize} from '../Utils/Normalize';
import {TextBold} from './Text';
import {BadgeProps} from '../Models/ComponentModels';

const Badge: React.FC<BadgeProps> = ({text}) => {
  return (
    <View style={styles.badge}>
      <TextBold type="S" text={text.toUpperCase()} color={Colors['Sky-600']} />
    </View>
  );
};

export default Badge;

const styles = StyleSheet.create({
  badge: {
    paddingHorizontal: scaleSize(16),
    paddingVertical: scaleSize(4),
    backgroundColor: Colors['Sky-50'],
    borderRadius: scaleSize(4),
    alignSelf: 'baseline',
  },
});
