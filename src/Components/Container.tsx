import React from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import {ContainerProps} from '../Models/ComponentModels';
import {Colors} from '../Themes/Colors';

const Container: React.FC<ContainerProps> = ({children, style}) => {
  return (
    <SafeAreaView style={[styles.safeArea, style]}>
      <View style={styles.container}>{children}</View>
    </SafeAreaView>
  );
};

export default Container;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Colors.White,
  },
  container: {
    flex: 1,
    backgroundColor: Colors.White,
  },
});
