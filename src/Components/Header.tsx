import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {IcArrowLeft} from '../Assets';
import {HeaderProps} from '../Models/ComponentModels';
import {Colors} from '../Themes/Colors';
import {scaleSize} from '../Utils/Normalize';
import {TextBold, TextSemiBold} from './Text';
import Gap from './Gap';
import NavigationServices from '../Utils/NavigationServices';

const Header: React.FC<HeaderProps> = ({
  type = 'brand-only',
  text = 'Brand Name',
}) => {
  const onBack = () => {
    NavigationServices.pop();
  };

  return type === 'with-back' ? (
    <View style={styles.headerBack}>
      <TouchableOpacity activeOpacity={0.7} onPress={onBack}>
        <IcArrowLeft />
      </TouchableOpacity>
      <Gap width={12} />
      <TextSemiBold type="L" text={text} />
    </View>
  ) : (
    <View style={styles.header}>
      <TextBold type="L" text={text} color={Colors['Sky-600']} />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  header: {
    padding: scaleSize(20),
  },
  headerBack: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: scaleSize(20),
  },
});
