import {Draft, produce} from 'immer';
import {SessionModel} from '../../Models/Stores/SessionModel';

const SessionActions = (set: any) => {
  return {
    setLogin: (isLogin: boolean) => {
      set(
        produce((draft: Draft<SessionModel>) => {
          draft.isLogin = isLogin;
        }),
      );
    },
  };
};

export default SessionActions;
