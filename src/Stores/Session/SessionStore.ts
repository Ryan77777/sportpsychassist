import {useStore} from 'zustand';
import {createJSONStorage, persist} from 'zustand/middleware';
import {StoreApi, createStore} from 'zustand/vanilla';
import {SessionModel} from '../../Models/Stores/SessionModel';
import MMKVServices from '../../Utils/MMKVServices';
import SessionActions from './SessionActions';

const InitialStore = {
  isLogin: false,
};

export const sessionStore = createStore<SessionModel>()(
  persist(
    set => ({
      ...InitialStore,
      ...SessionActions(set),
    }),
    {
      name: 'session-store',
      version: 1,
      storage: createJSONStorage(() => new MMKVServices('session-store')),
    },
  ),
);

const createBoundedUseStore = (store => (selector, equals) =>
  useStore(store, selector as never, equals)) as <S extends StoreApi<unknown>>(
  store: S,
) => {
  (): ExtractState<S>;
  <T>(
    selector: (state: ExtractState<S>) => T,
    equals?: (a: T, b: T) => boolean,
  ): T;
};

type ExtractState<S> = S extends {getState: () => infer X} ? X : never;

const useSessionStore = createBoundedUseStore(sessionStore);

export default useSessionStore;
