import {NativeModules} from 'react-native';
import {SplashScreenModule} from '../Models/ModulModels';

const SplashScreen: SplashScreenModule | undefined = NativeModules.SplashScreen;

function show() {
  SplashScreen?.show();
}

function hide() {
  SplashScreen?.hide();
}

export default {
  show,
  hide,
};
