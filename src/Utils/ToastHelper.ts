import {ShowToastProps, ToastInstance} from '../Models/UtilModels';

class ToastHelper {
  instance?: ToastInstance;

  constructor() {
    this.setInstance = this.setInstance.bind(this);
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
  }

  setInstance(instances: ToastInstance) {
    this.instance = instances;
  }

  show({title, message}: ShowToastProps) {
    if (this.instance) {
      this.instance.show(title, message);
    }
  }

  hide() {
    if (this.instance) {
      this.instance.hide();
    }
  }
}

export default new ToastHelper();
