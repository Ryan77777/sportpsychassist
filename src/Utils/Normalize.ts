import {Dimensions, PixelRatio} from 'react-native';

let screenWidth: number = Dimensions.get('window').width;
let screenHeight: number = Dimensions.get('window').height;

let initWidth: number = 375; // Ubah sesuai dengan lebar frame Figma Anda
let initHeight: number = 812; // Ubah sesuai dengan tinggi frame Figma Anda

const setInitValues = (width: number, height: number): void => {
  initWidth = width;
  initHeight = height;
  screenWidth = Dimensions.get('window').width;
  screenHeight = Dimensions.get('window').height;
};

const scaleSize = (
  value: number,
  based: 'width' | 'height' = 'width',
): number => {
  const newSize =
    based === 'height'
      ? PixelRatio.roundToNearestPixel((screenHeight / initHeight) * value)
      : PixelRatio.roundToNearestPixel((screenWidth / initWidth) * value);

  return newSize;
};

const scaleFont = (size: number): number => {
  const scale = Math.min(screenWidth / initWidth, screenHeight / initHeight);
  const adjustedSize = Math.ceil(size * scale);
  return PixelRatio.roundToNearestPixel(adjustedSize);
};

export {scaleSize, scaleFont, setInitValues};
