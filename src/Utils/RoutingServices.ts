import {register} from 'react-native-bundle-splitter';

export const LoginScreen = register({
  loader: () => require('../Screens/Auth/LoginScreen'),
  name: 'ChatRoomScreen',
});

export const HomeScreen = register({
  loader: () => require('../Screens/Main/HomeScreen'),
  name: 'HomeScreen',
});

export const MapsScreen = register({
  loader: () => require('../Screens/Main/MapsScreen'),
  name: 'MapsScreen',
});

export const ProfileScreen = register({
  loader: () => require('../Screens/Main/ProfileScreen'),
  name: 'ProfileScreen',
});

export const DetailScreen = register({
  loader: () => require('../Screens/Main/DetailScreen'),
  name: 'DetailScreen',
});

export const AddReportScreen = register({
  loader: () => require('../Screens/Main/AddReportScreen'),
  name: 'AddReportScreen',
});
