import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import useSessionStore from '../Stores/Session/SessionStore';
import NavigationServices from '../Utils/NavigationServices';
import {connect} from '../Utils/ZustandHelper';
import AuthNavigation from './AuthNavigation';
import MainNavigation from './MainNavigation';
import {SessionModel} from '../Models/Stores/SessionModel';

const RootNavigation = ({isLogin}: {isLogin: boolean}) => {
  return (
    <NavigationContainer
      ref={r => {
        NavigationServices.setInstance(r);
      }}>
      {isLogin ? <MainNavigation /> : <AuthNavigation />}
    </NavigationContainer>
  );
};

const sessionSelector = (state: SessionModel) => ({
  isLogin: state.isLogin,
});

const stores = [{store: useSessionStore, selector: sessionSelector}];

export default connect(stores)(RootNavigation);
