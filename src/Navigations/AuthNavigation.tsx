import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {LoginScreen} from '../Utils/RoutingServices';
import {AuthStackParams} from '../Models/NavigationModels';

const Stack = createNativeStackNavigator<AuthStackParams>();

const AuthNavigation = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        freezeOnBlur: true,
      }}>
      <Stack.Screen
        options={{headerShown: false}}
        name="LoginScreen"
        component={LoginScreen}
      />
    </Stack.Navigator>
  );
};

export default AuthNavigation;
