import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {MainStackParams} from '../Models/NavigationModels';
import {AddReportScreen, DetailScreen} from '../Utils/RoutingServices';
import MainTab from './MainTab';

const Stack = createNativeStackNavigator<MainStackParams>();

const MainNavigation = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        freezeOnBlur: true,
      }}>
      <Stack.Screen
        options={{headerShown: false}}
        name="MainScreen"
        component={MainTab}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="DetailScreen"
        component={DetailScreen}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="AddReportScreen"
        component={AddReportScreen}
      />
    </Stack.Navigator>
  );
};

export default MainNavigation;
