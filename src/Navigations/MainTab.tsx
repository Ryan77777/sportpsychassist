import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import {BottomNavigation} from '../Components';
import {MainTabParams} from '../Models/NavigationModels';
import {HomeScreen, MapsScreen, ProfileScreen} from '../Utils/RoutingServices';

const Tab = createBottomTabNavigator<MainTabParams>();

const MainTab: React.FC = () => {
  return (
    <Tab.Navigator tabBar={BottomNavigation}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="Maps"
        component={MapsScreen}
        options={{
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
};

export default MainTab;
