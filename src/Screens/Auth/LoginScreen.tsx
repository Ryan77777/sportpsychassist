import React, {useRef} from 'react';
import {useForm} from 'react-hook-form';
import {
  Container,
  FilledButton,
  Gap,
  Input,
  KeyboardAvoiding,
  TextBold,
  TextRegular,
} from '../../Components';
import {FormLoginData} from '../../Models/FormModels';
import {SessionModel} from '../../Models/Stores/SessionModel';
import useSessionStore from '../../Stores/Session/SessionStore';
import {Colors} from '../../Themes/Colors';
import LoadingHelper from '../../Utils/LoadingHelper';
import ToastHelper from '../../Utils/ToastHelper';
import {Keyboard, StyleSheet, TextInput} from 'react-native';
import {scaleSize} from '../../Utils/Normalize';

const LoginScreen = () => {
  const ref = useRef<TextInput>(null);

  const setLogin = useSessionStore((state: SessionModel) => state.setLogin);

  const {control, handleSubmit} = useForm<FormLoginData>({
    defaultValues: {
      username: '',
      password: '',
    },
  });

  const onSubmit = (data: FormLoginData) => {
    const expect = {
      username: 'Admin',
      password: 'Admin',
    };
    Keyboard.dismiss();
    LoadingHelper.show();

    setTimeout(() => {
      LoadingHelper.hide();
      if (
        data.username !== expect.username ||
        data.password !== expect.password
      ) {
        ToastHelper.show({
          title: 'Akun tidak ditemukan',
          message: 'Maaf, akun Anda tidak ditemukan atau password salah.',
        });
      } else {
        setLogin(true);
      }
    }, 2000);
  };

  return (
    <Container style={styles.container}>
      <KeyboardAvoiding>
        <TextBold type="3XL" text="Brand Name" color={Colors['Sky-600']} />
        <Gap height={48} />
        <TextBold type="XL" text="Sign In" color={Colors['Sky-600']} />
        <Gap height={4} />
        <TextRegular
          type="L"
          text="Sign in to access your account"
          color={Colors['Neutral-500']}
        />
        <Gap height={32} />
        <Input
          control={control}
          name="username"
          rules={{
            required: 'Username is required',
          }}
          label="Username"
          placeholder="Input your username"
          returnKeyType="next"
          onSubmitEditing={() => ref.current?.focus()}
        />
        <Gap height={16} />
        <Input
          ref={ref}
          control={control}
          name="password"
          rules={{
            required: 'Password is required',
          }}
          label="Password"
          placeholder="Input your password"
          secureTextEntry
          returnKeyType="done"
        />
        <Gap height={80} />
        <FilledButton text="Sign In" onPress={handleSubmit(onSubmit)} />
      </KeyboardAvoiding>
    </Container>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    padding: scaleSize(20),
  },
});
