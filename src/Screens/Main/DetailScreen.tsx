import React from 'react';
import {Dimensions, ScrollView, StyleSheet, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {
  Container,
  Gap,
  Header,
  TextMedium,
  TextRegular,
  TextSemiBold,
} from '../../Components';
import Maps from '../../Components/Screens/Detail/Maps';
import {Point} from '../../Data/Point';
import {Colors} from '../../Themes/Colors';
import {scaleSize} from '../../Utils/Normalize';

const DetailScreen = () => {
  return (
    <Container>
      <Header type="with-back" text="Detail" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <Maps latitude={-6.7030547} longitude={108.3570865} />
        <View style={styles.card}>
          <TextSemiBold type="L" text="Judul" />
          <Gap height={12} />
          <TextMedium type="S" text="Dibuat oleh Joko Susilo" />
          <Gap height={24} />
          <FastImage
            style={styles.thumbnail}
            source={{
              uri: 'https://picsum.photos/1280/720',
              priority: FastImage.priority.high,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
          <Gap height={16} />
          <TextSemiBold
            type="M"
            text="Poin-Poin dalam Pendampingan Psikologi"
          />
          <Gap height={12} />
          {Point.map((item, index) => (
            <View key={index} style={styles.list}>
              <View style={styles.dot} />
              <Gap width={4} />
              <TextRegular type="M" text={item} color={Colors['Neutral-600']} />
            </View>
          ))}
          <Gap height={16} />
          <TextSemiBold type="M" text="Kesimpulan Pendampingan" />
          <Gap height={12} />
          <TextRegular
            type="M"
            text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac dolor sit amet purus dignissim ultricies. Integer euismod bibendum libero, at malesuada nunc facilisis at. Fusce a dolor eget erat auctor gravida eu id libero. Nunc vel ex nec libero faucibus interdum. In hac habitasse platea dictumst."
            color={Colors['Neutral-600']}
            style={styles.text}
          />
        </View>
      </ScrollView>
    </Container>
  );
};

export default DetailScreen;

const styles = StyleSheet.create({
  card: {
    flex: 1,
    backgroundColor: Colors.White,
    marginTop: scaleSize(-24),
    borderTopEndRadius: scaleSize(16),
    borderTopStartRadius: scaleSize(16),
    padding: scaleSize(20),
    shadowColor: Colors.Black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 1,
  },
  thumbnail: {
    width: Dimensions.get('window').width - scaleSize(40),
    height: scaleSize(164),
    borderRadius: scaleSize(12),
  },
  list: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dot: {
    width: scaleSize(4),
    height: scaleSize(4),
    backgroundColor: Colors['Neutral-800'],
    borderRadius: scaleSize(4),
  },
  text: {textAlign: 'justify'},
});
