import React from 'react';
import {Container, Gap, Header, TextSemiBold} from '../../Components';
import Banner from '../../Components/Screens/Home/Banner';
import SectionReport from '../../Components/Screens/Home/SectionReport';
import {ScrollView, StyleSheet} from 'react-native';
import {scaleSize} from '../../Utils/Normalize';
import FAB from '../../Components/FAB';
import NavigationServices from '../../Utils/NavigationServices';

const HomeScreen = () => {
  const redirectTo = () => {
    NavigationServices.navigate('AddReportScreen');
  };

  return (
    <Container>
      <Header />
      <FAB onPress={redirectTo} />
      <ScrollView
        nestedScrollEnabled
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.container}>
        <Gap height={12} />
        <Banner />
        <Gap height={20} />
        <TextSemiBold type="L" text="Laporan Terkini" />
        <Gap height={16} />
        <SectionReport />
      </ScrollView>
    </Container>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: scaleSize(20),
  },
});
