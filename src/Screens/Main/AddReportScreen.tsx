import React from 'react';
import {Controller, useForm} from 'react-hook-form';
import {ScrollView, StyleSheet, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {launchImageLibrary} from 'react-native-image-picker';
import Animated, {FadeIn} from 'react-native-reanimated';
import {IcCamera, IcTrash} from '../../Assets';
import {
  Checkbox,
  Container,
  FilledButton,
  Gap,
  Header,
  Input,
  KeyboardAvoiding,
  TextRegular,
  TextSemiBold,
} from '../../Components';
import {Point} from '../../Data/Point';
import {FormAddReportData} from '../../Models/FormModels';
import {Colors} from '../../Themes/Colors';
import {scaleSize} from '../../Utils/Normalize';

const AddReportScreen = () => {
  const {control, handleSubmit, setValue, getValues} =
    useForm<FormAddReportData>({
      defaultValues: {
        title: '',
        selectedPoints: Point.map(() => false),
        description: '',
        photo: '',
      },
    });

  const onSubmit = (data: FormAddReportData) => {
    const selectedItems = Point.filter(
      (item, index) => data.selectedPoints[index] === true,
    );

    const dataForSubmit = {
      title: data.title,
      point: selectedItems,
      description: data.description,
    };

    console.log(dataForSubmit);
  };

  const chooseImage = async () => {
    await launchImageLibrary({mediaType: 'photo'}, (response: any) => {
      if (response.didCancel || response?.error) {
        console.log('oops, sepertinya anda tidak memilih foto nya?');
      } else {
        const source = {uri: response?.assets?.[0].uri};
        setValue('photo', source);
      }
    });
  };

  const deleteImage = () => {
    setValue('photo', '');
  };

  return (
    <Container>
      <Header type="with-back" text="Tambah Laporan" />
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.content}
        showsVerticalScrollIndicator={false}>
        <KeyboardAvoiding>
          <Input
            control={control}
            name="title"
            rules={{
              required: 'Wajib di isi',
            }}
            label="Judul"
            placeholder="Masukkan judul laporan"
            returnKeyType="done"
          />
          <Gap height={16} />
          <TextSemiBold
            type="M"
            text="Poin-Poin dalam Pendampingan Psikologi:"
          />
          <Gap height={12} />
          {Point.map((item, index) => (
            <View key={index} style={styles.checkbox}>
              <Controller
                control={control}
                name={`selectedPoints.${index + 1}`}
                render={({field: {value, onChange}}) => {
                  return (
                    <View style={styles.checkbox}>
                      <Checkbox
                        isCheck={value}
                        onPress={() => onChange(!value)}
                      />
                      <Gap width={12} />
                      <TextRegular
                        type="M"
                        text={item}
                        color={Colors['Neutral-600']}
                      />
                    </View>
                  );
                }}
              />
            </View>
          ))}
          <Gap height={16} />
          <Input
            control={control}
            name="description"
            rules={{
              required: 'Wajib di isi',
            }}
            label="Kesimpulan"
            placeholder="Masukkan kesimpulan"
            returnKeyType="done"
          />
          <Gap height={16} />
          <TextSemiBold type="M" text="Foto Laporan" />
          <Gap height={12} />
          <Controller
            control={control}
            name="photo"
            render={({field: {value}}) => {
              return value ? (
                <Animated.View
                  entering={FadeIn.duration(2000)}
                  style={styles.imagepickerContainer}>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.button}
                    onPress={deleteImage}>
                    <IcTrash />
                  </TouchableOpacity>
                  <FastImage
                    style={styles.profile}
                    source={{
                      uri: getValues('photo').uri,
                      priority: FastImage.priority.high,
                    }}
                    resizeMode={FastImage.resizeMode.cover}
                  />
                </Animated.View>
              ) : (
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={styles.imagepickerContainer}
                  onPress={chooseImage}>
                  <IcCamera />
                  <Gap height={4} />
                  <TextRegular
                    type="S"
                    text={'Upload\nmaksimal 5mb'}
                    style={styles.text}
                    color={Colors['Neutral-500']}
                  />
                </TouchableOpacity>
              );
            }}
          />
          <Gap height={16} />
          <FilledButton text="Simpan" onPress={handleSubmit(onSubmit)} />
        </KeyboardAvoiding>
      </ScrollView>
    </Container>
  );
};

export default AddReportScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: scaleSize(20),
    paddingBottom: scaleSize(100),
  },
  content: {
    paddingBottom: scaleSize(48),
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: scaleSize(12),
  },
  imagepickerContainer: {
    height: scaleSize(164),
    borderRadius: scaleSize(8),
    borderWidth: 1.5,
    borderStyle: 'dashed',
    borderColor: Colors['Neutral-200'],
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
  },
  profile: {
    width: '100%',
    height: scaleSize(164),
    borderRadius: scaleSize(8),
  },
  button: {
    position: 'absolute',
    top: 16,
    left: 16,
    backgroundColor: Colors.White,
    width: scaleSize(32),
    height: scaleSize(32),
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
});
