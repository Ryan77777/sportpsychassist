import React from 'react';
import {Platform, ScrollView} from 'react-native';
import {Container, FilledButton, Gap, Header} from '../../Components';
import CardProfile from '../../Components/Screens/Profile/CardProfile';
import Menu from '../../Components/Screens/Profile/Menu';
import {SessionModel} from '../../Models/Stores/SessionModel';
import useSessionStore from '../../Stores/Session/SessionStore';
import {scaleSize} from '../../Utils/Normalize';
import LoadingHelper from '../../Utils/LoadingHelper';

const ProfileScreen = () => {
  const {setLogin} = useSessionStore((state: SessionModel) => state);

  const onLogout = () => {
    LoadingHelper.show();

    setTimeout(() => {
      setLogin(false);
      LoadingHelper.hide();
    }, 2000);
  };
  return (
    <Container>
      <Header />
      <ScrollView
        nestedScrollEnabled
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingHorizontal: scaleSize(20),
          paddingBottom: Platform.OS === 'ios' ? scaleSize(64) : scaleSize(80),
        }}>
        <Gap height={20} />
        <CardProfile />
        <Gap height={20} />
        <Menu />
        <Gap height={20} />
        <FilledButton text="Logout" onPress={onLogout} />
      </ScrollView>
    </Container>
  );
};

export default ProfileScreen;
