import React, {useMemo} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {Container, Header} from '../../Components';

const markers = [
  {
    latitude: -6.836509,
    longitude: 108.220407,
    title: 'Marker 1',
    description: 'Deskripsi Marker 1',
  },
  {
    latitude: -6.738634,
    longitude: 108.32078,
    title: 'Marker 2',
    description: 'Deskripsi Marker 2',
  },
  {
    latitude: -6.773878,
    longitude: 108.471684,
    title: 'Marker 3',
    description: 'Deskripsi Marker 3',
  },
];

const MapsScreen = () => {
  const region = useMemo(() => {
    const minLat = Math.min(...markers.map(marker => marker.latitude));
    const maxLat = Math.max(...markers.map(marker => marker.latitude));
    const minLon = Math.min(...markers.map(marker => marker.longitude));
    const maxLon = Math.max(...markers.map(marker => marker.longitude));

    const midLat = (minLat + maxLat) / 2;
    const midLon = (minLon + maxLon) / 2;

    const deltaLat = maxLat - minLat + 0.02;
    const deltaLon = maxLon - minLon + 0.02;

    return {
      latitude: midLat,
      longitude: midLon,
      latitudeDelta: deltaLat,
      longitudeDelta: deltaLon,
    };
  }, []);
  return (
    <Container>
      <Header />
      <MapView style={styles.map} region={region}>
        {markers.map((marker, index) => (
          <Marker
            key={index}
            coordinate={{
              latitude: marker.latitude,
              longitude: marker.longitude,
            }}
            title={marker.title}
            description={marker.description}
          />
        ))}
      </MapView>
    </Container>
  );
};

export default MapsScreen;

const styles = StyleSheet.create({
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
