export const Point = [
  'Evaluasi Awal',
  'Penetapan Tujuan',
  'Pengembangan Keterampilan Mental',
  'Pemecahan Masalah',
  'Dukungan Emosional',
  'Komunikasi Tim',
  'Pemulihan Pasca Kompetisi',
  'Adaptasi Terhadap Perubahan',
  'Perencanaan Karir',
  'Kerahasiaan',
];
