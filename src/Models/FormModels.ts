export type FormLoginData = {
  username: string;
  password: string;
};

export type FormAddReportData = {
  title: string;
  selectedPoints: Array<boolean>;
  description: string;
  photo: any;
};
