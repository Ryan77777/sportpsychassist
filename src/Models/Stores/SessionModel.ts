export interface SessionModel extends SessionActionsProps {
  isLogin: boolean;
}

export interface SessionActionsProps {
  setLogin: (isLogin: boolean) => void;
}
