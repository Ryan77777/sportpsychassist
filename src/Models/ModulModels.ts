export type SplashScreenModule = {
  show: () => void;
  hide: () => void;
};
