import {NavigationState} from '@react-navigation/native';
import {ReactNode, RefObject} from 'react';
import {FieldError, FieldErrorsImpl, Merge} from 'react-hook-form';
import {TextInput, TextInputProps, TextStyle} from 'react-native';

export interface ContainerProps {
  children: ReactNode;
  style?: object;
}

export interface FilledButtonProps {
  text: string;
  paddingVertical?: number;
  borderRadius?: number;
  disabled?: boolean;
  onPress: () => void;
}

export interface LoadingProps {}

export interface LoadingState {
  visible: boolean;
}

export interface ToastProps {}

export interface ToastState {
  visible: boolean;
  title: string;
  message: string;
}

export interface KeyboardAvoidingProps {
  children: ReactNode;
}

export interface ErrorMessageProps {
  text:
    | string
    | FieldError
    | Merge<FieldError, FieldErrorsImpl<any>>
    | undefined;
  isError: boolean;
}

export interface GapProps {
  width?: number;
  height?: number;
}

export interface TextProps {
  style?: TextStyle;
  type: '6XL' | '5XL' | '4XL' | '3XL' | '2XL' | 'XL' | 'L' | 'M' | 'S' | 'XS';
  text: string;
  color?: string;
  numberOfLines?: number;
}

export interface InputProps extends TextInputProps {
  ref?: RefObject<TextInput>;
  label: string;
  control: any;
  name: string;
  rules?: Partial<Record<string, any>>;
}

export type MenuSection = {
  title: string;
  data: string[];
};

export interface BottomNavigationProps {
  state: NavigationState;
  descriptors: any;
  navigation: any;
}

export interface IconBottomNavigationProps {
  label: string;
  active: boolean;
}

export interface BadgeProps {
  text: string;
}

export interface CategoryOptionProps {
  text: string;
  isSelected: string;
  onPress: () => void;
}

export interface HeaderProps {
  type?: 'with-back' | 'brand-only';
  text?: string;
}

export interface MapsProps {
  latitude: number;
  longitude: number;
}

export interface FABProps {
  onPress: () => void;
}
