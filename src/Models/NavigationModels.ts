import {NavigationContainerRef} from '@react-navigation/native';

export type AuthStackParams = {
  LoginScreen: undefined;
};

export type MainStackParams = {
  MainScreen: undefined;
  DetailScreen: undefined;
  AddReportScreen: undefined;
};

export type MainTabParams = {
  Home: undefined;
  Maps: undefined;
  Profile: undefined;
};

type RootParamList = ReactNavigation.RootParamList;

export type NavigationRefType = NavigationContainerRef<RootParamList> | null;
