export interface LoadingInstance {
  show: () => void;
  hide: () => void;
}

export interface ToastInstance {
  show: (title: string, message: string) => void;
  hide: () => void;
}

export interface ShowToastProps {
  title: string;
  message: string;
}
