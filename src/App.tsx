import React, {useEffect} from 'react';
import {Toast} from './Components';
import Loading from './Components/Loading';
import SplashScreen from './Modules/SplashScreen';
import RootNavigation from './Navigations/RootNavigation';

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <>
      <RootNavigation />
      <Loading />
      <Toast />
    </>
  );
};

export default App;
